/*
 * analog.h
 *
 *  Created on: Nov 1, 2020
 *      Author: maria
 */

#ifndef INC_ANALOG_H_
#define INC_ANALOG_H_

#include "shell.h"
#include "FreeRTOS.h"
#include <stdio.h>
#include "cmsis_os2.h"


#define CMD_AD "ad"

BaseType_t ad_init();

BaseType_t de_init();

void ad_task_fn(const void *par);

void cmd_ad(int argc, char **argv);

uint8_t ad_get(uint32_t *result, TickType_t wait);

void ad_vis_fn(const void *par);

#endif /* INC_ANALOG_H_ */

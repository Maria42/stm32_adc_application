/*
 * analog.c
 *
 *  Created on: Nov 1, 2020
 *      Author: maria
 */

#include "shell.h"
#include "analog.h"
#include "string.h"
#include "task.h"
#include "semphr.h"
#include "stm32f4xx_hal_adc.h"
//#include "cmsis_os2.h"

#define CMD_HELP "help"
#define CMD_STAT "stat"
#define CMD_START "start"
#define CMD_STOP "stop"

#define CMD_VIS "vis"

#define CHANNELS (1)

#define AD_MAX_VALUE (4096)
#define AD_MIN_VALUE (1500)

#define NOTIFY_WAIT_MS (100)
#define CONV_INTERVAL_MS (20)

#define CONV_MULTIPLIER 1
#define CONV_DIVIDER 1
#define CONV_OFFSET 0

#define AD_TASK_SLEEP_TIME_MS (100)

#define EVT_AD_CONV_END (1<<0)
#define EVT_AD_CONV_START (1<<1)
#define EVT_AD_CONV_STOP (1<<2)
#define EVT_AD_MASK ((EVT_AD_CONV_END) | (EVT_AD_CONV_START) | (EVT_AD_CONV_STOP) )

//macros for moving average and hysteresis
#define MOVING_AVERAGE_QUEUE_SIZE 10
#define MOVING_HYSTERESIS_DELTA (10)

#define TASK_VIS_SLEEP_TIME_MS (100)
#define VISUALIZATION_MAX_VALUE 40
#define EVT_AD_VIS_STOP (1<<4)
#define EVT_AD_VIS_START (1<<3)
#define EVT_VIS_MASK ((EVT_AD_VIS_START)|(EVT_AD_VIS_STOP))


extern ADC_HandleTypeDef hadc1;
osThreadId_t ADTaskHandle;
extern osMutexId_t mtx_adHandle;
extern osThreadId_t VISTaskHandle;




//struct for moving hysteresis
typedef struct{
   uint32_t delta;
   uint32_t hyst_min;
   uint32_t hyst_max;
}moving_hysteresis_t;

//struct for moving average
typedef struct{
   uint32_t queue[MOVING_AVERAGE_QUEUE_SIZE];
   uint8_t index;
   uint32_t average;
}moving_average_t;

typedef struct{
      uint32_t start_error;
      uint32_t stop_error;
      uint32_t conv_success;
      uint32_t conv_error;
}ad_statistics_t;

typedef struct{
     volatile uint8_t running;
     volatile uint8_t vis_running;
     uint32_t raw_value;
     uint32_t current_value;
     ad_statistics_t statistics;

     moving_hysteresis_t moving_hysteresis;
     moving_average_t moving_average;
}ad_task_ctrl_t;


static ad_task_ctrl_t ad_task_ctrl;


BaseType_t ad_init(){
     bzero(&ad_task_ctrl, sizeof(ad_task_ctrl_t));
     ad_task_ctrl.moving_hysteresis.hyst_min=0;
     ad_task_ctrl.moving_hysteresis.hyst_max=MOVING_HYSTERESIS_DELTA;
     ad_task_ctrl.moving_hysteresis.delta=MOVING_HYSTERESIS_DELTA;

     return pdPASS;
}

BaseType_t de_init(){
	return pdPASS;
}





void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc){
     ++ad_task_ctrl.statistics.conv_success;
     BaseType_t xHigherPriorityTaskWoken = pdFALSE;
     xTaskNotifyFromISR(ADTaskHandle,EVT_AD_CONV_END, eSetBits, &xHigherPriorityTaskWoken);

     portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc) {
        HAL_ADC_Stop_DMA(hadc);
        ++ad_task_ctrl.statistics.conv_error;
}


static void start_ad_conversion(){
	   BaseType_t result;
	   result = HAL_ADC_Start_DMA(&hadc1, &ad_task_ctrl.raw_value, CHANNELS);

	   if(result != HAL_OK)
		  ++ad_task_ctrl.statistics.start_error;
	   else
		   ad_task_ctrl.running=1;

}


static uint32_t add_moving_hysteresis(uint32_t input){
       if(input > ad_task_ctrl.moving_hysteresis.hyst_max){
    	   ad_task_ctrl.moving_hysteresis.hyst_max += ad_task_ctrl.moving_hysteresis.delta;
    	   ad_task_ctrl.moving_hysteresis.hyst_min += ad_task_ctrl.moving_hysteresis.delta;
    	   return ad_task_ctrl.moving_hysteresis.hyst_max;
       }

       if(input < ad_task_ctrl.moving_hysteresis.hyst_min){
           ad_task_ctrl.moving_hysteresis.hyst_min -= ad_task_ctrl.moving_hysteresis.delta;
           ad_task_ctrl.moving_hysteresis.hyst_max -= ad_task_ctrl.moving_hysteresis.delta;
           return ad_task_ctrl.moving_hysteresis.hyst_max;
       }
       return input;
}

static void add_moving_average_value(uint32_t input){
    ad_task_ctrl.moving_average.queue[ad_task_ctrl.moving_average.index]=input;
    ad_task_ctrl.moving_average.index=(ad_task_ctrl.moving_average.index+1)%MOVING_AVERAGE_QUEUE_SIZE;
    uint32_t sum=0;
    for(uint8_t i=0;i<MOVING_AVERAGE_QUEUE_SIZE;++i)
    	sum += ad_task_ctrl.moving_average.queue[i];

    ad_task_ctrl.moving_average.average =sum/MOVING_AVERAGE_QUEUE_SIZE;
}


void ad_task_fn(const void *par){
    uint32_t notified_value;
    BaseType_t result;
    for(;;){
    	result=xTaskNotifyWait(EVT_AD_MASK,EVT_AD_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

    	if(notified_value & EVT_AD_CONV_STOP){
           ad_task_ctrl.running=0;
           result = HAL_ADC_Stop_DMA(&hadc1);
           if(result != HAL_OK){
               ++ad_task_ctrl.statistics.stop_error;
               pr_err("AD Stop error");

           }
    	}

    	if(notified_value & EVT_AD_CONV_START){
          start_ad_conversion();
    	}

        if(notified_value & EVT_AD_CONV_END){

          add_moving_average_value(add_moving_hysteresis(ad_task_ctrl.raw_value));

          if (xSemaphoreTake(mtx_adHandle, AD_TASK_SLEEP_TIME_MS/2)==pdTRUE) {
                ad_task_ctrl.current_value = ad_task_ctrl.moving_average.average    //ad_tsk_ctrl.ad_raw_value[0]
                                                 * CONV_MULTIPLIER / CONV_DIVIDER + CONV_OFFSET;
                ++ad_task_ctrl.statistics.conv_success;
                xSemaphoreGive(mtx_adHandle);
          }
          pr_debug("current value:%d", ad_tsk_ctrl.current_value);

          //memcpy(&ad_task_ctrl.raw_value, &ad_task_ctrl.current_value, sizeof(ad_task_ctrl.raw_value)); //TODO:  MOVING AVERAGE & MOVING HYSTERYSIS

          osDelay(CONV_INTERVAL_MS);
          if(!ad_task_ctrl.running)
        	  continue;
          start_ad_conversion();
        }
    }
}


static void cmd_usage(){
   sh_printf("usage: %s < help | stat | start | vis < start | stop > > \r\n",CMD_AD);
}

void cmd_ad(int argc, char **argv){
    if (argc<=0 || strcasecmp(CMD_HELP, argv[0])==0) {
            cmd_usage();
            return;
    }


     if(strcasecmp(CMD_STAT, argv[0])==0){
        sh_printf("state is %s \r\n", ad_task_ctrl.running ? "started" : "stopped");

        sh_printf("Conversion: %d, Conv Errors: %d, Start Errors: %d, Stop Errors: %d\r\n",ad_task_ctrl.statistics.conv_success, ad_task_ctrl.statistics.conv_error, ad_task_ctrl.statistics.start_error, ad_task_ctrl.statistics.stop_error);
        sh_printf("Current value is %d, raw value is: %d\r\n",ad_task_ctrl.moving_average.average,ad_task_ctrl.raw_value);
        return;
     }


     if(strcasecmp(CMD_START,argv[0])==0){
         if(ad_task_ctrl.running){
        	sh_printf("AD task already running\r\n");
        	return;
         }
         xTaskNotify(ADTaskHandle, EVT_AD_CONV_START, eSetBits);
         bzero(&ad_task_ctrl.statistics,sizeof(ad_statistics_t));
         sh_printf("AD Task is starting \r\n");
         return;
     }

     if(strcasecmp(CMD_STOP,argv[0])==0){
    	if(!ad_task_ctrl.running){
    		sh_printf("AD task already stopped\r\n");
    		return;
    	}
        xTaskNotify(ADTaskHandle, EVT_AD_CONV_STOP, eSetBits);
        sh_printf("AD Task is stopping \r\n");
        return;
     }

     if(argc==2 && strcasecmp(CMD_VIS,argv[0])==0){
        if(strcasecmp(CMD_START,argv[1])==0){
        	if(ad_task_ctrl.vis_running){
        		sh_printf("visualization already running\r\n");
        		return;
        	}
        	xTaskNotify(VISTaskHandle,EVT_AD_VIS_START,eSetBits);
        	sh_printf("visualization is starting\r\n");
        	return;
        }
        if(strcasecmp(CMD_STOP,argv[1])==0){
        	if(!ad_task_ctrl.vis_running){
        		sh_printf("visualization already stopped\r\n");
        		return;
        	}
            xTaskNotify(VISTaskHandle,EVT_AD_VIS_STOP,eSetBits);
        	sh_printf("Visualization task is stopping\r\n");
        	return;
        }
     }
     cmd_usage();
}

uint8_t ad_get(uint32_t *result, TickType_t wait){
        assert_param(result);
        if(xSemaphoreTake(mtx_adHandle,wait)==pdTRUE){
           *result=ad_task_ctrl.moving_average.average;
           xSemaphoreGive(mtx_adHandle);
           return pdPASS;
        }
        return pdFAIL;
}


static inline uint32_t min(uint32_t a,uint32_t b){
	return a<b?a:b;
}


static inline void vis_value(char *buffer,uint32_t v){
    bzero(buffer,VISUALIZATION_MAX_VALUE+1);
    memset(buffer, '=',v);
    pr_info(buffer);
}


void ad_vis_fn(const void *par){
     char visual_buffer[VISUALIZATION_MAX_VALUE+1];

     for(;;){
        uint32_t notified_value=0;
        xTaskNotifyWait(0, EVT_VIS_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

        if(notified_value & EVT_AD_VIS_START){
        	ad_task_ctrl.vis_running=1;
        }
        if(notified_value & EVT_AD_VIS_STOP){
        	ad_task_ctrl.vis_running=0;
        	vis_value(visual_buffer, 0);
        	goto tsk_wait;
        }
        if(ad_task_ctrl.vis_running){
        	uint32_t current=0;
        	if(!ad_get(&current, osWaitForever)){
        		goto tsk_wait;
        	}
        	current=(current-AD_MIN_VALUE)*VISUALIZATION_MAX_VALUE/(AD_MAX_VALUE-AD_MIN_VALUE);
            uint32_t size=min(VISUALIZATION_MAX_VALUE, current);
            vis_value(visual_buffer, size);
        }

tsk_wait:
        osDelay(TASK_VIS_SLEEP_TIME_MS);


     }
}



